#!/bin/sh
set -e

if [ -z "$1" ] || [ -z "$2" ]; then
  echo "Error: PROFILE or BACKEND_PORT is not set. Run script with args provided './run.sh PROFILE BACKEND_PORT'"
  exit 1
fi

PROFILE=$1
BACKEND_PORT=$2

java -jar ./back-end/CodeRythmApi/build/libs/CodeRythmApi.jar --spring.profiles.active="$PROFILE" &
JAVA_PID=$!
JAVA_STATUS=$?

# Add a try-catch block
set +e
trap "echo 'Script interrupted.'; kill -9 $JAVA_PID; exit $JAVA_STATUS" INT TERM EXIT

# Wait for the server to start
while ! nc -z localhost $BACKEND_PORT; do
  if [ $JAVA_STATUS -ne 0 ]; then
    echo "Error: Java process exited with status $JAVA_STATUS"
    exit $JAVA_STATUS
  fi
  echo "Waiting for backend(with '$PROFILE'profile) to start at 'localhost:$BACKEND_PORT'"
  sleep 1
done

# Catch any errors and kill the running server before exiting with a non-zero status code
if [ $? -ne 0 ]; then
  echo "Error: Backend start failed with exit status $?"
  kill -9 $JAVA_PID
  exit $?
fi

# Remove the trap
trap - INT TERM EXIT
echo "Backend Server started successfully at 'localhost:$BACKEND_PORT'!"
# if you want to kill the running server, user the:
# $ps aux | grep java
# $kill -9 id-of-java-process
