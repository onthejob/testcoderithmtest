package com.codecool.coderythmapi.service;

import com.codecool.coderythmapi.model.TestModel;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD) // clear db before each test
class TestServiceTest {
    @Autowired
    TestService testService;

    @Autowired
    EntityManager entityManager;


    @Test
    void create() {
        String message = "Model created from test";
        TestModel testModel = testService.create(message);
        TestModel testModelFromDb = entityManager.find(TestModel.class, 1);
        assertEquals(testModel.getMessage(), testModelFromDb.getMessage());
    }
}