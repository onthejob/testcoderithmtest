package com.codecool.coderythmapi.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@SequenceGenerator(name = "test_model_seq", allocationSize = 1)
@Getter
@Setter
@NoArgsConstructor
public class TestModel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "test_model_seq")
    @Column(name = "id", nullable = false)
    private Integer id;


    private String message;

    public TestModel(String message) {
        this.message = message;
    }
}
