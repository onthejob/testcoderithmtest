package com.codecool.coderythmapi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
@AllArgsConstructor
public class TestDTO {
    private Integer id;
    private String message;
}