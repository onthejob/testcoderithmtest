package com.codecool.coderythmapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodeRythmApiApplication {

	public static void main(String[] args) {SpringApplication.run(CodeRythmApiApplication.class, args);}

}
