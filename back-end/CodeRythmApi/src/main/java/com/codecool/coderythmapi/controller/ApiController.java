package com.codecool.coderythmapi.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
This abstract controller ensures the extended class to all endpoint call starts with '/api'
 **/
@RestController
@RequestMapping("/api")
public abstract class ApiController {
}
