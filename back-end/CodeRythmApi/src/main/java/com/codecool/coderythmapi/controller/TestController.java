package com.codecool.coderythmapi.controller;


import com.codecool.coderythmapi.api.TestOpenApiApi;
import com.codecool.coderythmapi.dto.TestDTO;
import com.codecool.coderythmapi.model.ApiTestModelDTO;
import com.codecool.coderythmapi.service.TestService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import com.codecool.coderythmapi.model.TestModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequiredArgsConstructor
public class TestController extends ApiController implements TestOpenApiApi {
    private final TestService testService;

    @GetMapping("test")
    public TestDTO testCORS() {
        System.out.println("'ap/test' ROUT CALLED");
        TestModel testModel = testService.create(String.format("Test model: %s", LocalDate.now()));
        return TestDTO.builder()
                .message(testModel.getMessage())
                .id(testModel.getId())
                .build();
    }

    @Override
    public ResponseEntity<ApiTestModelDTO> getTestResult() {
        return ResponseEntity.ok(ApiTestModelDTO.builder().message("Hello from server !").age(1).build());
    }


}
