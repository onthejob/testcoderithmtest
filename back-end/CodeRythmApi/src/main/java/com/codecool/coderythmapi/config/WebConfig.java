package com.codecool.coderythmapi.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.beans.factory.annotation.Value;


@Configuration
@Slf4j
public class WebConfig implements WebMvcConfigurer {

    @Value("${dev.frontend.url}")
    private String frontend_url;
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        log.info(String.format("Cors allowed origin: %s", frontend_url));
        registry.addMapping("/api/**")
                .allowedOrigins(frontend_url)
                .allowedMethods("GET", "POST", "PUT", "DELETE")
                .allowedHeaders("*");
    }
}
