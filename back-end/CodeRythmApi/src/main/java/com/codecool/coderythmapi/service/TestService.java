package com.codecool.coderythmapi.service;

import com.codecool.coderythmapi.model.TestModel;
import com.codecool.coderythmapi.repository.TestRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TestService {
    private final TestRepository testRepository;


    public TestModel create(String message) {
        return testRepository.save(new TestModel(message));
    }
}
