package com.codecool.coderythmapi.controller;

import com.codecool.coderythmapi.api.UserApi;
import com.codecool.coderythmapi.model.UserDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController extends ApiController implements UserApi {

    @GetMapping("/hello")
    public void hello() {
        System.out.println("Hello");
    }


    @Override
    public ResponseEntity<UserDTO> getUserName() {
        UserDTO userDTO = UserDTO.builder().name("Hello from server Im the openApi DTO").build();
        return ResponseEntity.ok(userDTO);
    }
}
