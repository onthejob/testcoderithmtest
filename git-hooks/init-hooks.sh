#!/bin/sh

if ! which node > /dev/null
    then
        echo "Node required!"
        echo "No node is installed, skipping..."
        exit 1
    fi

GREEN='\033[0;32m'
BLUE='\033[0;34m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color
PROJECT_ROOT_DIR="$(git rev-parse --show-toplevel)"
GIT_HOOKS_PATH="$PROJECT_ROOT_DIR/.git/hooks/"
CURRENT_DIR="$PROJECT_ROOT_DIR/git-hooks/"
COMMIT_MSG_FILE=commit-msg
PRE_PUSH_FILE=pre-push
CUSTOM_HOOK_SAMPLE_NAME=-sample
COMMIT_HOOK_MESSAGE="From now You have to follow the commit and branch naming convention"
PRE_PUSH_MESSAGE="From now on, git will runs tests before each 'push'..." # We are poor and our repo has only 400 min CI credit\month
PRE_PUSH_AVAILABLE=true
echo "${YELLOW}Before setup, Please read this description about these custom hooks:${NC} https://dpmap.atlassian.net/wiki/spaces/~6321747d55a96b85c311daea/pages/2031617/Git+hooks+in+Our+project"
read -p "To continue, press Enter."
echo "${BLUE}Create/rewrite 'commit-msg' custom hook${NC}"

if test -f "$GIT_HOOKS_PATH$COMMIT_MSG_FILE"; then
    # set nocasematch option
    shopt -s nocasematch

    while ! [[ $isRewriteComitMsg =~ ((y|yes|n|no))+$ ]]; do
      echo "${YELLOW}$COMMIT_MSG_FILE custom hook already exists. Do you want to rewrite?${NC}"
      read -p "Enter [y/n]: " isRewriteComitMsg
    done

    if [[ $isRewriteComitMsg =~ ((y|yes))+$ ]];
      then
        # remove the old commit-msg file
        rm $GIT_HOOKS_PATH$COMMIT_MSG_FILE

        # copy the commit-msg-sample into git hooks dir and rename it to commit-msg
        cp $CURRENT_DIR$COMMIT_MSG_FILE$CUSTOM_HOOK_SAMPLE_NAME $GIT_HOOKS_PATH$COMMIT_MSG_FILE
        echo "${GREEN}'$COMMIT_MSG_FILE' custom hook is updated!${NC}"
        echo "${GREEN}$COMMIT_HOOK_MESSAGE${NC}"
      else
        echo "${BLUE}$COMMIT_MSG_FILE hook does not installed!${NC}"
    fi
    # unset nocasematch option
    shopt -u nocasematch

    else
      cp $CURRENT_DIR$COMMIT_MSG_FILE$CUSTOM_HOOK_SAMPLE_NAME $GIT_HOOKS_PATH$COMMIT_MSG_FILE
      echo "${GREEN}$COMMIT_MSG_FILE custom hook has successfully created.${NC}"
      echo "${GREEN}$COMMIT_HOOK_MESSAGE${NC}"
fi

if $PRE_PUSH_AVAILABLE ; then
  echo "--------------------------------------------"
  echo "${BLUE}Create/rewrite 'pre-push' custom hook${NC}"
  if test -f "$GIT_HOOKS_PATH$PRE_PUSH_FILE" ; then
      # set nocasematch option
      shopt -s nocasematch

      while ! [[ $is_rewrite_pre_push =~ ((y|yes|n|no))+$ ]]; do
        echo "${YELLOW}$PRE_PUSH_FILE custom hook already exists. Do you want to rewrite?${NC}"
        read -p "Enter [y/n]: " is_rewrite_pre_push
      done

      if [[ $is_rewrite_pre_push =~ ((y|yes))+$ ]];
        then
          # remove the old pre-push file
          rm $GIT_HOOKS_PATH$PRE_PUSH_FILE

          # copy the pre-push-sample into git hooks dir and rename it to pre-push
          cp $CURRENT_DIR$PRE_PUSH_FILE$CUSTOM_HOOK_SAMPLE_NAME $GIT_HOOKS_PATH$PRE_PUSH_FILE
          echo "${GREEN}'$PRE_PUSH_FILE' custom hook is updated!${NC}"
          echo "${GREEN}$PRE_PUSH_MESSAGE${NC}"
        else
          echo "${BLUE}$PRE_PUSH_FILE hook does not installed!${NC}"
      fi
      # unset nocasematch option
      shopt -u nocasematch

      else
        cp $CURRENT_DIR$PRE_PUSH_FILE$CUSTOM_HOOK_SAMPLE_NAME $GIT_HOOKS_PATH$PRE_PUSH_FILE
        echo "${GREEN}$PRE_PUSH_FILE custom hook has successfully created.${GREEN}"
        echo "${GREEN}$PRE_PUSH_MESSAGE${NC}"
  fi
fi

