const express = require('express');
const app = express();
const path = require('path');
require('dotenv').config();

const projectDir = process.env.PROJECT_DIR || 'default';

app.use(express.static(path.join(__dirname, projectDir)));
// app.use(express.static(path.join(__dirname, projectDir, 'public')));

app.listen(4200, () => {
    console.log(`Server listening on port 4200 for ${projectDir} project`);
});
