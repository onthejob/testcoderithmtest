import {Configuration, ConfigurationParameters} from "./open_api";
import {environment} from "../environments/environment";


export function apiConfigFactory(): Configuration {
  const params: ConfigurationParameters = {
    basePath: environment.basePath + environment.apiEndpointPrefix,
  };
  return new Configuration(params);
}
