import { Injectable } from '@angular/core';
import {HttpBackend, HttpClient} from "@angular/common/http";
import {catchError, Observable, throwError} from "rxjs";
import {TestDTO} from "../dto/test-dto";

@Injectable({
  providedIn: 'root'
})
export class MainPageService {
  private httpWithoutInterceptor: HttpClient;

  constructor(
    private http: HttpClient,
    private httpBackend: HttpBackend
  ) {
    this.httpWithoutInterceptor = new HttpClient(httpBackend);
  }

  private formatErrors(error: any) {
    return throwError(error.error);
  }

  public getTest(): Observable<TestDTO> {
    return this.httpWithoutInterceptor.get<TestDTO>('/api/test')
      .pipe(catchError(this.formatErrors));
  }
}
