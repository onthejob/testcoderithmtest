import {Component, OnInit} from '@angular/core';
import {MainPageService} from "./service/main-page.service";
import {TestDTO} from "./dto/test-dto";
import {ApiApiTestModelDTO, TestEndpointService, UserDetailsService, ApiUserDTO} from "../../open_api";
import {Observable} from "rxjs";

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit{

  apiTestModel: ApiApiTestModelDTO | undefined;
  $testDto: Observable<TestDTO> = this.mainPageService.getTest();
  userDetail: ApiUserDTO | undefined;


  constructor(
    private mainPageService: MainPageService,
    private testEnd: TestEndpointService,
    private userDetailService: UserDetailsService
    ) {
  }

  ngOnInit(): void {
    this.userDetailService.getUserName().subscribe((userDTOResult) => {
      this.userDetail = userDTOResult;
    })



    this.testEnd.getTestResult().subscribe({
      next: res => this.apiTestModel = res,
      error: err => {}// handle the error somehow
    });
  }
}
