import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './routing/app-routing.module';
import {RouterModule} from "@angular/router";
import {ApiModule} from "./open_api";
import { MainPageComponent } from './components/main-page/main-page.component';
import {HttpClientModule} from "@angular/common/http";
import {apiConfigFactory} from "./api-config";

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent
  ],
    imports: [
      ApiModule.forRoot(apiConfigFactory),
        BrowserModule,
        AppRoutingModule,
        RouterModule,
      HttpClientModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
