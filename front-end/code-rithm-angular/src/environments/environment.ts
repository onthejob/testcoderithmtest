export const environment = {
  production: false,
  basePath: 'http://localhost:8080',
  apiEndpointPrefix: '/api'
};
