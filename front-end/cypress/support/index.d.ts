declare namespace Cypress {
    interface Chainable<Subject> {
        /**
         * Logs the subject to the console using the specified console method.
         * @example
         * cy.get('button').console('info').then(($button) => {
         *   // subject is still $button
         * })
         */
        console(method?: string): Chainable<Subject>
    }
}