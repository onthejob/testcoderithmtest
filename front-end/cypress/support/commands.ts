// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
declare namespace Cypress {
    interface Chainable<Subject> {
        /**
         * Logs the subject to the console using the specified console method.
         * @example
         * cy.get('button').console('info').then(($button) => {
         *   // subject is still $button
         * })
         */
        dataCy(selector?: string): Chainable<Subject>
    }
}
Cypress.Commands.add('dataCy', (selector: string) => {
    return cy.get(`[data-cy="${selector}"]`);
});