import {testConfig} from "./test.config";

const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    supportFile: "./support/e2e.ts",
    specPattern: "./**/*.cy.ts",
    excludeSpecPattern: "./e2e/example/**/*.cy.ts", // cypress will ignore all example files
    ...testConfig
  },
  video: true,
  videoUploadOnPasses: false,
  videosFolder: "./dist/record/video",
  screenshotsFolder: "./dist/record/screenshot",
  screenshotOnRunFailure: true
});
