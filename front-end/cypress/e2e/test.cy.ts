/// <reference types="cypress" />

describe('Test main page', () => {
    beforeEach(() => {
        // Cypress starts out with a blank slate for each test
        // so we must tell it to visit our website with the `cy.visit()` command.
        // Since we want to visit the same URL at the start of all our tests,
        // we include it in our beforeEach function so that it runs before each test
        cy.visit('/');
        // cy.visit(Cypress.env('appUrl'))
    })

    it.only('should load in', function () {
        cy.dataCy('api-message').should('exist').should('contain', 'Hello from server Im the openApi DTO');
    });
})
