ARG CHROME_VERSION='113.0.5672.63-1'

# Cypress version has to be the same as ./front-end/cypress/package.json
ARG CYPRESS_VERSION='12.11.0'


# https://hub.docker.com/r/cypress/factory
FROM cypress/factory:latest
RUN mkdir -p /usr/share/man/man1/ && \
    apt-get update && \
    apt-get install -y netcat && \
    apt-get install -y openjdk-17-jre-headless && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
#
WORKDIR /app